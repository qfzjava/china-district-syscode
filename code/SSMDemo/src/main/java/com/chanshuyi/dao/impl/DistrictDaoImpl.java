package com.chanshuyi.dao.impl;

import com.chanshuyi.dao.IDistrictDao;
import com.chanshuyi.dao.mapper.DistrictMapper;
import com.chanshuyi.model.District;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DistrictDaoImpl extends BaseDao implements IDistrictDao{

    @Override
    public List<District> getUserListByMapSql(Map<String, String> param) {
        DistrictMapper mapper = readonlySQLSession.getMapper(DistrictMapper.class);
        return mapper.getDistrictListByMapSql(new HashMap<String, String>());
    }
}
