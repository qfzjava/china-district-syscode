package com.chanshuyi.dao.impl;

import com.chanshuyi.dao.ISysCodeDao;
import com.chanshuyi.dao.mapper.SysCodeMapper;
import com.chanshuyi.model.SysCode;

public class SysCodeDaoDaoImpl extends BaseDao implements ISysCodeDao {
    @Override
    public void insert(SysCode sysCode) {
        SysCodeMapper sysCodeMapper = readonlySQLSession.getMapper(SysCodeMapper.class);
        sysCodeMapper.insert(sysCode);
    }
}
