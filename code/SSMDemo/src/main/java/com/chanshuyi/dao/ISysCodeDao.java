package com.chanshuyi.dao;

import com.chanshuyi.model.SysCode;

public interface ISysCodeDao {
    void insert(SysCode sysCode);
}
