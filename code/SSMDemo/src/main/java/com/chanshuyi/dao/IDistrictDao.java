package com.chanshuyi.dao;

import com.chanshuyi.model.District;

import java.util.List;
import java.util.Map;

public interface IDistrictDao {
    List<District> getUserListByMapSql(Map<String, String> param);
}
