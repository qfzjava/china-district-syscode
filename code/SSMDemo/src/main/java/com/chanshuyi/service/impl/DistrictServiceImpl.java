package com.chanshuyi.service.impl;

import com.chanshuyi.dao.IDistrictDao;
import com.chanshuyi.model.District;
import com.chanshuyi.service.IDistrictService;

import java.util.List;
import java.util.Map;

public class DistrictServiceImpl implements IDistrictService{

    IDistrictDao districtDao;

    @Override
    public List<District> getUserListByMapSql(Map<String, String> param) {
        return districtDao.getUserListByMapSql(param);
    }

    public IDistrictDao getDistrictDao() {
        return districtDao;
    }

    public void setDistrictDao(IDistrictDao districtDao) {
        this.districtDao = districtDao;
    }
}
