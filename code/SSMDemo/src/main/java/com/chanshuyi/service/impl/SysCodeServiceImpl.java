package com.chanshuyi.service.impl;

import com.chanshuyi.dao.ISysCodeDao;
import com.chanshuyi.model.SysCode;
import com.chanshuyi.service.ISysCodeService;

public class SysCodeServiceImpl implements ISysCodeService {

    ISysCodeDao sysCodeDao;

    @Override
    public void insert(SysCode sysCode) {
        sysCodeDao.insert(sysCode);
    }

    public ISysCodeDao getSysCodeDao() {
        return sysCodeDao;
    }

    public void setSysCodeDao(ISysCodeDao sysCodeDao) {
        this.sysCodeDao = sysCodeDao;
    }
}
