package com.chanshuyi.model;

public class District {
    /**
     *
     */
    private Short id;

    /**
     *
     */
    private String name;

    /**
     *
     */
    private Short parent_id;

    /**
     *
     */
    private String initial;

    /**
     *
     */
    private String initials;

    /**
     *
     */
    private String pinyin;

    /**
     *
     */
    private String extra;

    /**
     *
     */
    private String suffix;

    /**
     *
     */
    private String code;

    /**
     *
     */
    private String area_code;

    /**
     *
     */
    private Byte order;

    /**
     *
     */
    public Short getId() {
        return id;
    }

    /**
     *
     */
    public void setId(Short id) {
        this.id = id;
    }

    /**
     *
     */
    public String getName() {
        return name;
    }

    /**
     *
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     *
     */
    public Short getParent_id() {
        return parent_id;
    }

    /**
     *
     */
    public void setParent_id(Short parent_id) {
        this.parent_id = parent_id;
    }

    /**
     *
     */
    public String getInitial() {
        return initial;
    }

    /**
     *
     */
    public void setInitial(String initial) {
        this.initial = initial == null ? null : initial.trim();
    }

    /**
     *
     */
    public String getInitials() {
        return initials;
    }

    /**
     *
     */
    public void setInitials(String initials) {
        this.initials = initials == null ? null : initials.trim();
    }

    /**
     *
     */
    public String getPinyin() {
        return pinyin;
    }

    /**
     *
     */
    public void setPinyin(String pinyin) {
        this.pinyin = pinyin == null ? null : pinyin.trim();
    }

    /**
     *
     */
    public String getExtra() {
        return extra;
    }

    /**
     *
     */
    public void setExtra(String extra) {
        this.extra = extra == null ? null : extra.trim();
    }

    /**
     *
     */
    public String getSuffix() {
        return suffix;
    }

    /**
     *
     */
    public void setSuffix(String suffix) {
        this.suffix = suffix == null ? null : suffix.trim();
    }

    /**
     *
     */
    public String getCode() {
        return code;
    }

    /**
     *
     */
    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    /**
     *
     */
    public String getArea_code() {
        return area_code;
    }

    /**
     *
     */
    public void setArea_code(String area_code) {
        this.area_code = area_code == null ? null : area_code.trim();
    }

    /**
     *
     */
    public Byte getOrder() {
        return order;
    }

    /**
     *
     */
    public void setOrder(Byte order) {
        this.order = order;
    }
}