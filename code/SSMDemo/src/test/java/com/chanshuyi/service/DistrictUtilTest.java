package com.chanshuyi.service;

import com.chanshuyi.model.District;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DistrictUtilTest {

    ApplicationContext ctx = null;

    IDistrictService districtService;

    @Before
    public void init() {
        ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        districtService = (IDistrictService) ctx.getBean("districtService");
    }

    @Test
    public void test() {
        List<District> districtList = districtService.getUserListByMapSql(new HashMap<String, String>());

        System.out.println("列表：" + districtList.size());

        Map<String, District> provinceMap = new HashMap<>();
        Map<String, District> cityMap = new HashMap<>();
        Map<String, District> districtMap = new HashMap<>();
        Map<String, District> TownMap = new HashMap<>();
        Map<String, District> otherMap = new HashMap<>();

        for (District district : districtList) {
            String suffix = district.getSuffix();
            if (suffix.equals("省") || suffix.equals("自治区") || suffix.equals("直辖市") || suffix.equals("特别行政区")) {
                provinceMap.put(district.getCode(), district);
            } else if (suffix.equals("市") || suffix.equals("地区") || suffix.equals("自治州") || suffix.equals("盟")) {
                cityMap.put(district.getCode(), district);
            } else if (suffix.equals("区") || suffix.equals("县") || suffix.equals("自治县") || suffix.equals("旗") || suffix.equals("自治旗")) {
                districtMap.put(district.getCode(), district);
            } else if(suffix.equals("乡") || suffix.equals("镇") || suffix.equals("行委")){
                TownMap.put(district.getCode(), district);
            } else {
                otherMap.put(district.getCode(), district);
            }
        }

        for (String key : otherMap.keySet()) {
            System.out.println(key + ":" + otherMap.get(key).getName());
        }

        System.out.println("总共：" + (provinceMap.size() + cityMap.size() + districtMap.size()));



        System.out.println("Hello");
    }
}
