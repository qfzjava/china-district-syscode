# 说明

- 配置数据库

创建MySQL数据库：superdemo，创建表：user。

```
create database superdemo;
use superdemo;
CREATE TABLE `user` (
`id`  int NULL ,
`username`  varchar(255) NULL ,
`password`  varchar(255) NULL,
PRIMARY KEY (ID)
);
insert into user values(1,"admin","admin");
```

修改数据库地址文件：jdbc.properties，修改数据库地址、用户名、密码。

- 配置maven命令：tomcat7:run 启动
- 打开浏览器访问：http://localhost:5050/index.jsp