# 中国行政区划编码

本项目致力于提供具有层级关系的行政区划数据。

相对其他的项目，本项目具有**准确性、实时性**的特点。

- **准确性。** 为了使数据更为准确，项目根据[国家统计局](http://www.stats.gov.cn/)公布的行政区划代码数据，对行政区划数据进行了整理，使其具有父子级关系，便于进行开发。
- **实时性。** 为了使数据不过于滞后，项目将定时根据[国家统计局](http://www.stats.gov.cn/)公布的行政区划代码数据，对项目数据文件进行更新。

## 背景知识

在使用这个项目之前，了解一下中国行政区划相关的概念还是很有必要的。

中国的行政区划可以划分为**省级、地级、县级、乡级**4个级别：

- 省级。省级主要包括了：省、自治区、直辖市、特别行政区四种类型。
- 地级。地级主要包括了：市、自治州、盟等。
- 县级。县级主要包括了：市辖区、县级市、县、区、自治县、省直辖县等。
- 镇级。镇级主要包括了：镇、乡等级别，是中国行政区划基层单位。

除了了解行政区划划分之外，我们需要特别注意以下几个概念。

**直辖市**

直辖市是省级单位，包括了：北京、上海、天津、重庆4个。

**省直辖县**

省直辖县是县级单位，但是其直接上级并不是地级市，而是由省级单位直接管理，或者由地级行政区划（地级市）代管。目前只有海南、新疆、江苏、河南、湖北、台湾省存在省直辖县。

正是因为存在特殊的层级关系，所以在国家统计局给出的数据中，你会发现省直辖县的上级是一个名字为`省直辖县级行政区划`的虚拟上级。

![](img/01_district_syscode_county.png)

**市辖区**

市辖区是县级行政单位，与县级市、县、区是一个级别。市辖区一般只存在于发达的城市，一般为城市核心区域的县级管理单位。

![](img/02_district_syscode_city.png)

## 如何使用

项目对行政区划的层级关系进行了梳理，将其直接导入了一个名为SysCode的数据库表。SysCode数据的表结构如下：

|字段名|描述|含义|
|:---:|:---:|:---:|
|codeId|编码ID|对应国家行政区划编码|
|codeTypeId|编码类型ID|对应国家行政区划等级|
|codeName|编码名称|对应国家行政区划名称|
|parentCodeId|父级编码ID|对应国家行政区划编码|

其中国家行政区划等级分为4个级别，分别是：国家级（数字1）、省级（数字2）、地级（数字3）、县级（数字4）。

使用本项目，直接下载导出的SQL备份文件[SysCode.sql](data/SysCode.sql)，然后执行SQL即可。

- *在本项目的行政区划数据只细化到县级单位。*
- *国家统计局没有给出中国的行政区划编码，本项目将其设置为100000。*

## 最新动态

|时间|内容|
|:---:|:---:|
|2017-07-13|根据[国家统计局 - 最新县及县以上行政区划代码（截止2016年7月31日）](http://www.stats.gov.cn/tjsj/tjbz/xzqhdm/201703/t20170310_1471429.html)整理行政区划数据|

## 参考资料

- [1].百度百科.[中国行政区划_百度百科](http://baike.baidu.com/item/%E4%B8%AD%E5%9B%BD%E8%A1%8C%E6%94%BF%E5%8C%BA%E5%88%92).[DB/OL].2017-07-13
- [2].中华人民共和国国家统计局.[中华人民共和国国家统计局](http://www.stats.gov.cn/).[DB/OL].2017-07-13 
